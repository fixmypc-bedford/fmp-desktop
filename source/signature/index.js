const { app, BrowserWindow, ipcMain } = require('electron');

const path = require('path');

app.on('ready', ()=>{
    const window = new BrowserWindow();
    window.loadURL(path.join(__dirname, 'index.html'));

    const SerialPort = require('serialport');
    
    SerialPort.list()
    .then( ports => {
        console.log(ports);
    });
    
    var port = new SerialPort('COM15', { baudRate: 19200 }, function (err) {
        if (err) {
            return console.log('Error: ', err.message);
        }
    });
    
    
    let buff = [];
    let ticking = false;
    
    // Start Bytes
    // 128
    // 193
    // 192
    
    port.on('readable', function () {
        let data = port.read();
        //console.log('Data:', data, data.length);
    
        for (let int of data) {
            if (int == 192 || int == 193 || int == 128) {
                ticking = true;
                //console.log("Enabled ticking");
            }
    
            if (ticking) {
                //console.log("Appending Byte", int);
                buff.push(int);
            } else {
                //console.log("Discarding Byte", int);
                process.exit();
            }
    
            if (buff.length == 5) {
                console.log("Pushing buff");
                tick(buff);
                buff = [];
                ticking = false;
            }
        }
    });
    
    function tick(d) {
        console.log("New data:", d);
        let x = d[1] * d[2];
        let y = d[3] * d[4];
        console.log(`Assumed: ${x} x ${y}`);
        window.webContents.send('data', d);
    }

});