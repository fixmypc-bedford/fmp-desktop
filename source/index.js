const { app, BrowserWindow, ipcMain } = require('electron');

let dev = false;

const path = require('path');
const fs = require('fs');

const defaultConfiguration = {
    labelServer: {
        host: '127.0.0.1',
        port: 8090,
        version: 1
    }
};
const configurationFile = path.join(app.getPath('userData'), 'configuration.json');

let window = null;

app.on('ready', function(){
    console.log(process.argv);
    if (process.argv.indexOf('-dev') > -1) dev=true;

    let configuration = defaultConfiguration;

    new Promise( (resolve, reject) => {
        if (fs.existsSync(configurationFile)) {
            console.log(`Loading Configuration from ${configurationFile}`);
            fs.readFile(configurationFile, (err, data) => {
                if (err) return reject(err);
                try {
                    configuration = JSON.parse(data.toString());
                    resolve();
                } catch(e) {
                    reject(e);
                }
            });
        } else {
            console.log(`Saving default configuration to ${configurationFile}`);
            fs.writeFile(configurationFile, JSON.stringify(configuration), err => {
                if (err) return reject(`Error saving default configuration! ${err}`)
                resolve();
            });
        }
    })
    .then( () => {
        window = new BrowserWindow({
            frame: dev
        });
        if (!dev) window.setMenu(null);
        window.loadURL(path.join(__dirname, 'app.html'));
        window.webContents.send('configuration', configuration);
        ipcMain.on('configuration', (ev, arg) => {
            ev.returnValue = configuration;
        });
    });
});

app.on('certificate-error', (event, webContents, url, error, certificate, callback) => {
    console.log(`Accepting invalid certificate for ${url}`);
    event.preventDefault();
    callback(true);
});